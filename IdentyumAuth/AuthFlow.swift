//
//  AuthState.swift
//  IdentyumAuth
//
//  Created by Katarina Pelicaric on 17.05.2021..
//

import Foundation
import AppAuth

public enum Environment: String {
    case dev
    case stage
    case live
}

public typealias AuthState = OIDAuthState
public typealias AuthStateChangeDelegate = OIDAuthStateChangeDelegate
public typealias AuthorizationService = OIDAuthorizationService
public typealias TokenResponse = OIDTokenResponse

public class AuthFlow {
        
    private var configuration: OIDServiceConfiguration!
    private var request: OIDAuthorizationRequest!

    public var currentAuthorizationFlow: OIDExternalUserAgentSession?
    public var authState: OIDAuthState?
    
    public init(clientID: String, redirectURL: URL?, scopes: [String], environment: Environment = .stage, additionalParameters: [String: String]) {
        guard let redirect = redirectURL else {
            print("Error creating URL for redirect")
            return
        }
        let authorizationEndpoint = URL(string: "https://web-components.\(environment).identyum.com/authorize")!
        let tokenEndpoint = URL(string: "https://identifier.\(environment).identyum.com/api/v1/auth/token")!
        
        configuration = OIDServiceConfiguration(authorizationEndpoint: authorizationEndpoint,
                                                    tokenEndpoint: tokenEndpoint)
        
        request = OIDAuthorizationRequest(configuration: configuration,
                                              clientId: clientID,
                                              scopes: scopes,
                                              redirectURL: redirect,
                                              responseType: OIDResponseTypeCode,
                                              additionalParameters: additionalParameters)
    }
    
    public func authState(presenting presentingViewController: UIViewController, callback: @escaping OIDAuthStateAuthorizationCallback) -> OIDExternalUserAgentSession {
        return OIDAuthState.authState(byPresenting: request, presenting: presentingViewController, callback: callback)
    }
}
