![logo](https://docs.identyum.com/img/logo.png)

IdentyumAuth for iOS is a client SDK for communicating with 
[OAuth 2.0](https://tools.ietf.org/html/rfc6749) and 
[OpenID Connect](http://openid.net/specs/openid-connect-core-1_0.html) providers. 
It strives to
directly map the requests and responses of those specifications, while following
the idiomatic style of the implementation language. In addition to mapping the
raw protocol flows, convenience methods are available to assist with common
tasks like performing an action with fresh tokens.

It follows the best practices set out in 
[RFC 8252 - OAuth 2.0 for Native Apps](https://tools.ietf.org/html/rfc8252)
including using `SFAuthenticationSession` and `SFSafariViewController` on iOS
for the auth request. `UIWebView` and `WKWebView` are explicitly *not*
supported due to the security and usability reasons explained in
[Section 8.12 of RFC 8252](https://tools.ietf.org/html/rfc8252#section-8.12).


## Specification

### iOS

#### Supported Versions

AppAuth supports iOS 7 and above.

iOS 9+ uses the in-app browser tab pattern
(via `SFSafariViewController`), and falls back to the system browser (mobile
Safari) on earlier versions.

#### Authorization Server Requirements

Both Custom URI Schemes (all supported versions of iOS) and Universal Links
(iOS 9+) can be used with the library.

In general, IdentyumAuth can work with any authorization server that supports
native apps, as documented in [RFC 8252](https://tools.ietf.org/html/rfc8252),
either through custom URI scheme redirects, or universal links.
Authorization servers that assume all clients are web-based, or require clients to maintain
confidentiality of the client secrets may not work well.


## Setup

### CocoaPods

With [CocoaPods](https://guides.cocoapods.org/using/getting-started.html),
add the following line to your `Podfile`:

    pod 'IdentyumAuth',  :git => 'https://bitbucket.org/identyum/identyum-auth-ios/'

Then, run `pod install`.


## Auth Flow

IdentyumAuth supports both manual interaction with the authorization server
where you need to perform your own token exchanges, as well as convenience
methods that perform some of this logic for you. This example uses the
convenience method, which returns either an `OIDAuthState` object, or an error.

`OIDAuthState` is a class that keeps track of the authorization and token
requests and responses, and provides a convenience method to call an API with
fresh tokens. This is the only object that you need to serialize to retain the
authorization state of the session.


### Authorizing – iOS

First, you need to have a property in your `UIApplicationDelegate`
implementation to hold the session and the auth state, in order to continue the authorization flow
from the redirect. In this example, the implementation of this delegate is
a class named `AppDelegate`, if your app's application delegate has a different
name, please update the class name in samples below accordingly.

<sub>Swift</sub>
```swift
class AppDelegate: UIResponder, UIApplicationDelegate {
  // property of the app's AppDelegate
  var authFlow: AuthFlow!
  
  // your clientID
  private let clientID = "test_client_sms"

  private let redirectURL = URL(string: "myapp://oauth-redirect")
  
  // example for scopes
  let scopes = ["id_video_check", "id_scan"]
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
     // Override point for customization after application launch.
        
     authFlow = AuthFlow(clientID: clientID, redirectURL: redirectURL, scopes: scopes, environment: .dev)
        
     return true
  }
}
```


Then, initiate the authorization request. By using the 
`authStateByPresentingAuthorizationRequest` convenience method, the token
exchange will be performed automatically, and everything will be protected with
PKCE (if the server supports it). AppAuth also lets you perform these
requests manually. See the `authNoCodeExchange` method in the included Example
app for a demonstration:

<sub>Swift</sub>
```swift
let appDelegate = UIApplication.shared.delegate as! AppDelegate

// performs authentication request
print("Initiating authorization request with scope: \(appDelegate.scopes)")

appDelegate.authFlow.currentAuthorizationFlow =
    appDelegate.authFlow.authState(presenting: self, callback: { (newAuthState, error) in
        if let authState = newAuthState {
             self.appDelegate.authFlow.authState = authState
             print("Got authorization tokens. Access token: " +
                        "\(authState.lastTokenResponse?.accessToken ?? "nil")")
             self.setupHomeView()
        } else {
             print("Authorization error: \(error?.localizedDescription ?? "Unknown error")")
             self.appDelegate.authFlow.authState = nil
        }
   })
}
```


### Making API Calls

IdentyumAuth gives you the raw token information, if you need it. However, we
recommend that users of the `OIDAuthState` convenience wrapper use the provided
`performActionWithFreshTokens:` method to perform their API calls to avoid
needing to worry about token freshness:

<sub>Swift</sub>
```swift
// your client secret
let clientSecret = "example"
let userinfoEndpoint = URL(string:"https://identifier.dev.identyum.com/api/v1/identity/personal-data?includeImages=false")!
appDelegate.authFlow.authState?.performAction() { (accessToken, idToken, error) in

  if error != nil  {
    print("Error fetching fresh tokens: \(error?.localizedDescription ?? "Unknown error")")
    return
  }
  guard let accessToken = accessToken else {
    return
  }

  var urlRequest = URLRequest(url: userinfoEndpoint)
  urlRequest.allHTTPHeaderFields = ["Authorization": "Bearer \(accessToken)",
                                    "accept": "application/json",
                                    "X-SECRET-KEY": clientSecret]

  // Perform request...
}
```

### Included example

Check `Example` project to see how to save `AuthState` through whole application.