//
//  LivenessCheckViewController.swift
//  Example
//
//  Created by Katarina Pelicaric on 02.06.2021..
//

import UIKit
import SnapKit
import IdentyumAuth

class LivenessCheckViewController: UIViewController {
    
    private let sessionManager = SessionManager.shared
    private let imageView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaInsets.top).offset(80)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().inset(20)
            make.bottom.lessThanOrEqualToSuperview().inset(50)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadLivenessCheckData()
    }
    
    private func loadLivenessCheckData() {
        let userinfoEndpoint = URL(string:"https://identifier.dev.identyum.com/api/v1/identity/liveness-check")!
        
        sessionManager.authState?.performAction() { (accessToken, idToken, error) in
            if error != nil  {
                print("Error fetching fresh tokens: \(error?.localizedDescription ?? "Unknown error")")
                return
            }
            guard let accessToken = accessToken else {
                return
            }

            var urlRequest = URLRequest(url: userinfoEndpoint)
            urlRequest.allHTTPHeaderFields = ["Authorization": "Bearer \(accessToken)",
                                              "accept": "application/json"]

            let task = URLSession.shared.dataTask(with: urlRequest) { [weak self] (data, response, error) in
                guard let data = data, error == nil else { return }
                guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                    print("Server error!")
                    return
                }

                if let livenessCheck = try? JSONDecoder().decode(LivenessCheck.self, from: data) {
                    self?.setImage(with: livenessCheck)
                }
            }
            task.resume()
        }
    }
    
    private func setImage(with data: LivenessCheck) {
        DispatchQueue.main.async { [weak self] in
            let dataDecoded : Data = Data(base64Encoded: data.faceImage, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            self?.imageView.image = decodedimage
        }
    }
}
