//
//  LoginViewController.swift
//  Example
//
//  Created by Katarina Pelicaric on 25.05.2021..
//

import UIKit
import SnapKit
import IdentyumAuth

class LoginViewController: UIViewController {
    
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let sessionManager = SessionManager.shared
    private let logoImageView = UIImageView()
    private let loginButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationController?.isNavigationBarHidden = true
        
        logoImageView.image = UIImage(named: "identyumlogo")
        logoImageView.contentMode = .scaleAspectFill
        view.addSubview(logoImageView)
        logoImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(120)
        }
        
        loginButton.setTitle("Login", for: .normal)
        loginButton.setTitleColor(.black, for: .normal)
        loginButton.layer.cornerRadius = 20
        loginButton.backgroundColor = .lightGray
        view.addSubview(loginButton)
        loginButton.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 200, height: 50))
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        loginButton.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
    }
    
    @objc func loginTapped() {
        // performs authentication request
        print("Initiating authorization request with scope: \(appDelegate.scopes)")
        
        appDelegate.authFlow.currentAuthorizationFlow = appDelegate.authFlow.authState(presenting: self, callback: { (newAuthState, error) in
            if let authState = newAuthState {
                print("Got authorization tokens. Access token: " +
                        "\(authState.lastTokenResponse?.accessToken ?? "nil")")
                self.sessionManager.saveState(state: authState)
                self.showTabBarController()
            } else {
                print("Authorization error: \(error?.localizedDescription ?? "Unknown error")")
                self.sessionManager.saveState(state: nil)
            }
        })
    }
    
    private func showTabBarController() {
        appDelegate.changeRootViewControllerToTabBar()
    }
}
