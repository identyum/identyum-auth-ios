//
//  AppDelegate.swift
//  Example
//
//  Created by Katarina Pelicaric on 25.05.2021..
//

import UIKit
import IdentyumAuth

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var authFlow: AuthFlow!
    let scopes = ["id_video_check", "id_scan"]
    private let clientID = "test_client_sms"
    private let redirectURL = URL(string: "myapp://oauth-redirect")

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        authFlow = AuthFlow(clientID: clientID, redirectURL: redirectURL, scopes: scopes, environment: .dev)

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let sessionManager = SessionManager.shared
        if (sessionManager.authState != nil) {
            window?.rootViewController = tabController
        } else {
            window?.rootViewController = UINavigationController(rootViewController: LoginViewController())
        }
        
        return true
    }
    
    private lazy var tabController: UITabBarController = {
        let tabViewController = UITabBarController()
        
        let personalDataVC = PersonalDataViewController()
        let personalDataItem = UITabBarItem()
        personalDataItem.title = "Personal Data"
        personalDataItem.image = UIImage(named: "home")
        personalDataVC.tabBarItem = personalDataItem
        
        let livenessCheckVC = LivenessCheckViewController()
        let livenessCheckItem = UITabBarItem()
        livenessCheckItem.title = "Liveness Check"
        livenessCheckItem.image = UIImage(named: "camera")
        livenessCheckVC.tabBarItem = livenessCheckItem
        
        let profileVC = ProfileViewController()
        let profileItem = UITabBarItem()
        profileItem.title = "Profile"
        profileItem.image = UIImage(named: "user")
        profileVC.tabBarItem = profileItem
        
        tabViewController.viewControllers = [personalDataVC, livenessCheckVC, profileVC]
        tabViewController.selectedViewController = personalDataVC
        
        return tabViewController
    }()
    
    func changeRootViewController(_ vc: UIViewController, animated: Bool = true) {
        guard let window = self.window else {
            return
        }
        window.rootViewController = vc
    }
    
    func changeRootViewControllerToTabBar(animated: Bool = true) {
        guard let window = self.window else {
            return
        }
        window.rootViewController = tabController
        tabController.selectedIndex = 0
    }
}
