//
//  SessionManager.swift
//  Example
//
//  Created by Katarina Pelicaric on 02.06.2021..
//

import Foundation
import IdentyumAuth

class SessionManager: NSObject {
    
    static let shared = SessionManager()
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let authStateKey = "authStateKey"
    private var _authState: AuthState?
    
    func saveState(state: AuthState?) {
        guard let state = state else {
            logout()
            return
        }
        _authState = state
        _authState?.stateChangeDelegate = self
        
        let data = try? NSKeyedArchiver.archivedData(withRootObject: state, requiringSecureCoding: true)
        UserDefaults.standard.setValue(data, forKey: authStateKey)
    }
    
    var authState: AuthState? {
        if let state = _authState {
            return state
        }
        guard let data = UserDefaults.standard.object(forKey: authStateKey) as? Data else {
            return nil
        }

        if let authState = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? AuthState {
            return authState
        }
        return nil
    }
    
    func logout() {
        _authState = nil
        UserDefaults.standard.setValue(nil, forKey: authStateKey)
        appDelegate.changeRootViewController(LoginViewController())
    }
}

extension SessionManager: AuthStateChangeDelegate {
    
    func didChange(_ state: AuthState) {
        saveState(state: state)
    }
}
