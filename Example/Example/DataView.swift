//
//  DataView.swift
//  Example
//
//  Created by Katarina Pelicaric on 25.05.2021..
//

import UIKit

class DataView: UIView {
    
    private let leftLabel = UILabel()
    private let rightLabel = UILabel()
    private let imageView = UIImageView()
    private var imageHeight: CGFloat = 0.0
    
    var uppercased: Bool? {
        didSet {
            if uppercased == true {
                leftLabel.text = leftLabel.text?.uppercased()
            }
        }
    }

    init(leftText: String, rightText: String) {
        leftLabel.text = leftText.capitalized
        rightLabel.text = rightText.capitalized
        super.init(frame: .zero)
        setupViewWithText()
    }
    
    init(leftText: String, image: UIImage?, imageHeight: CGFloat) {
        leftLabel.text = leftText.capitalized
        imageView.image = image
        self.imageHeight = imageHeight
        super.init(frame: .zero)
        setupViewWithImage()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViewWithText() {
        leftLabel.textColor = .black
        leftLabel.numberOfLines = 0
        addSubview(leftLabel)
        leftLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12)
            make.left.equalToSuperview().offset(12)
            make.bottom.lessThanOrEqualToSuperview().inset(20)
            make.width.equalTo(150)
        }
        
        rightLabel.textColor = .black
        rightLabel.numberOfLines = 0
        addSubview(rightLabel)
        rightLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12)
            make.right.equalToSuperview().inset(12)
            make.bottom.equalToSuperview().inset(20)
            make.left.equalTo(leftLabel.snp.right).offset(12)
        }
    }
    
    private func setupViewWithImage() {
        leftLabel.textColor = .black
        leftLabel.numberOfLines = 0
        addSubview(leftLabel)
        leftLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12)
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview()
        }
        
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(leftLabel.snp.bottom).offset(10)
            make.left.equalToSuperview().offset(12)
            make.right.equalToSuperview().inset(12)
            make.height.equalTo(imageHeight)
            make.bottom.equalToSuperview().inset(20)
        }
    }
}
