//
//  PersonalData.swift
//  Example
//
//  Created by Katarina Pelicaric on 25.05.2021..
//

import Foundation

struct PersonalData: Codable {
    let addresses: [ValueNestedObject]
    let dateOfBirth: Value
    let firstName: Value
    let lastName: Value
    let fullName: Value
    let nationalityCode: Value
    let sex: Value
    let personalNumbers: [ValueNestedObject]
    let documents: [Document]
}

struct Value: Codable {
    let value: String
}

struct ValueNestedObject: Codable {
    let value: ValueTypeObject
}

struct ValueTypeObject: Codable {
    let type: String
    let value: String
}

struct Document: Codable {
    let frontImage: DocumentImage
    let backImage: DocumentImage
    let signatureImage: DocumentImage
    let dateOfExpiry: Value
    let dateOfIssue: Value
    let type: Value
    let issuingCountryCode: Value
    let number: Value
    let issuedBy: Value
    
    struct DocumentImage: Codable {
        let id: String
        let base64Value: String
    }
}


struct ProfileData: Codable {
    let userUuid: String
    let phones: [ValueTypeObject]?
    let emails: [ValueTypeObject]?
}


struct LivenessCheck: Codable {
    let faceImage: String
}
