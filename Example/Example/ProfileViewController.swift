//
//  ProfileViewController.swift
//  Example
//
//  Created by Katarina Pelicaric on 02.06.2021..
//

import UIKit
import SnapKit
import IdentyumAuth

class ProfileViewController: UIViewController {
    
    private let sessionManager = SessionManager.shared
    private let logoutButton = UIButton()
    private let stackView = UIStackView()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        logoutButton.setTitle("Logout", for: .normal)
        logoutButton.setTitleColor(.black, for: .normal)
        view.addSubview(logoutButton)
        logoutButton.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaInsets.top).offset(80)
            make.centerX.equalToSuperview()
        }
        logoutButton.addTarget(self, action: #selector(logoutTapped), for: .touchUpInside)
        
        stackView.axis = .vertical
        view.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(logoutButton.snp.bottom).offset(20)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.lessThanOrEqualToSuperview().inset(50)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadProfile()
    }
    
    @objc func logoutTapped() {
        sessionManager.logout()
    }
    
    private func loadProfile() {
        let userinfoEndpoint = URL(string:"https://identifier.dev.identyum.com/api/v1/identity/profile")!
        
        sessionManager.authState?.performAction() { (accessToken, idToken, error) in
            if error != nil  {
                print("Error fetching fresh tokens: \(error?.localizedDescription ?? "Unknown error")")
                return
            }
            guard let accessToken = accessToken else {
                return
            }

            var urlRequest = URLRequest(url: userinfoEndpoint)
            urlRequest.allHTTPHeaderFields = ["Authorization": "Bearer \(accessToken)",
                                              "accept": "application/json"]

            let task = URLSession.shared.dataTask(with: urlRequest) { [weak self] (data, response, error) in
                guard let data = data, error == nil else { return }
                guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                    print("Server error!")
                    return
                }

                if let profile = try? JSONDecoder().decode(ProfileData.self, from: data) {
                    self?.setViews(with: profile)
                }
            }
            task.resume()
        }
    }
    
    private func setViews(with data: ProfileData) {
        DispatchQueue.main.async { [weak self] in
            self?.stackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })

            let titleView = DataView(leftText: "PROFILE INFORMATION", rightText: "")
            titleView.uppercased = true
            self?.stackView.addArrangedSubview(titleView)
            
            let userUuidView = DataView(leftText: "user UUID", rightText: data.userUuid)
            self?.stackView.addArrangedSubview(userUuidView)
            
            if let phones = data.phones {
                let phonesView = DataView(leftText: "Phones:", rightText: "")
                self?.stackView.addArrangedSubview(phonesView)
                
                for phone in phones {
                    let phoneView = DataView(leftText: phone.type, rightText: phone.value)
                    self?.stackView.addArrangedSubview(phoneView)
                }
            }
            
            if let emails = data.emails {
                let emailsView = DataView(leftText: "Emails:", rightText: "")
                self?.stackView.addArrangedSubview(emailsView)
                
                for email in emails {
                    let emailView = DataView(leftText: email.type, rightText: email.value)
                    self?.stackView.addArrangedSubview(emailView)
                }
            }
        }
    }
}
