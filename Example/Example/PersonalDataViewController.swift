//
//  PersonalDataViewController.swift
//  Example
//
//  Created by Katarina Pelicaric on 02.06.2021..
//

import UIKit
import SnapKit
import IdentyumAuth

class PersonalDataViewController: UIViewController {
    
    private let sessionManager = SessionManager.shared
    private let scrollView = UIScrollView()
    private let contentView = UIView()
    private let stackView = UIStackView()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        stackView.axis = .vertical
        contentView.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaInsets.top).offset(80)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.lessThanOrEqualToSuperview().inset(50)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadUserData()
    }
    
    private func loadUserData() {
        let clientSecret = "12345"
        let userinfoEndpoint = URL(string:"https://identifier.dev.identyum.com/api/v1/identity/personal-data?includeImages=true")!
                
        sessionManager.authState?.performAction(freshTokens: { (accessToken, idToken, error) in
            if error != nil  {
                print("Error fetching fresh tokens: \(error?.localizedDescription ?? "Unknown error")")
                return
            }

            guard let accessToken = accessToken else {
                return
            }
            
            var urlRequest = URLRequest(url: userinfoEndpoint)
            urlRequest.allHTTPHeaderFields = ["Authorization": "Bearer \(accessToken)",
                                              "accept": "application/json",
                                              "X-SECRET-KEY": clientSecret]

            let task = URLSession.shared.dataTask(with: urlRequest) { [weak self] (data, response, error) in
                guard let data = data, error == nil else { return }
                guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                    print("Server error!")
                    return
                }

                if let personalData = try? JSONDecoder().decode(PersonalData.self, from: data) {
                    self?.setViews(with: personalData)
                }
            }
            task.resume()
        })
    }
    
    private func setViews(with data: PersonalData) {
        DispatchQueue.main.async { [weak self] in
            self?.stackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })

            let titleView = DataView(leftText: "PERSONAL DATA", rightText: "")
            titleView.uppercased = true
            self?.stackView.addArrangedSubview(titleView)
            
            let firstNameView = DataView(leftText: "firstName", rightText: data.firstName.value)
            self?.stackView.addArrangedSubview(firstNameView)
            
            let lastNameView = DataView(leftText: "lastName", rightText: data.lastName.value)
            self?.stackView.addArrangedSubview(lastNameView)
            
            let birthDateView = DataView(leftText: "date of birth", rightText: data.dateOfBirth.value)
            self?.stackView.addArrangedSubview(birthDateView)
            
            for address in data.addresses {
                let addressView = DataView(leftText: address.value.type, rightText: address.value.value)
                self?.stackView.addArrangedSubview(addressView)
            }
            
            let nationalityView = DataView(leftText: "nationality code", rightText: data.nationalityCode.value)
            self?.stackView.addArrangedSubview(nationalityView)
            
            for number in data.personalNumbers {
                let numberView = DataView(leftText: number.value.type, rightText: number.value.value)
                self?.stackView.addArrangedSubview(numberView)
            }
            
            for document in data.documents {
                let frontImageData: Data = Data(base64Encoded: document.frontImage.base64Value, options: .ignoreUnknownCharacters)!
                let frontImage = UIImage(data: frontImageData)
                let frontImageView = DataView(leftText: "Front image", image: frontImage, imageHeight: 150)
                self?.stackView.addArrangedSubview(frontImageView)
                
                let backImageData: Data = Data(base64Encoded: document.backImage.base64Value, options: .ignoreUnknownCharacters)!
                let backImage = UIImage(data: backImageData)
                let backImageView = DataView(leftText: "Back image", image: backImage, imageHeight: 150)
                self?.stackView.addArrangedSubview(backImageView)

                let signatureImageData: Data = Data(base64Encoded: document.signatureImage.base64Value, options: .ignoreUnknownCharacters)!
                let signatureImage = UIImage(data: signatureImageData)
                let signatureImageView = DataView(leftText: "Signature image", image: signatureImage, imageHeight: 50)
                self?.stackView.addArrangedSubview(signatureImageView)
                
                let dateOfIssueView = DataView(leftText: "Date Of Issue", rightText: document.dateOfIssue.value)
                self?.stackView.addArrangedSubview(dateOfIssueView)
                
                let dateOfExpiryView = DataView(leftText: "Date Of Expiry", rightText: document.dateOfExpiry.value)
                self?.stackView.addArrangedSubview(dateOfExpiryView)
                
                let typeView = DataView(leftText: "Type", rightText: document.type.value)
                self?.stackView.addArrangedSubview(typeView)
                
                let issuingCountryCodeView = DataView(leftText: "Issuing Country Code", rightText: document.issuingCountryCode.value)
                self?.stackView.addArrangedSubview(issuingCountryCodeView)
                
                let issuedByView = DataView(leftText: "Issued By", rightText: document.issuedBy.value)
                self?.stackView.addArrangedSubview(issuedByView)
                
                let numberView = DataView(leftText: "Number", rightText: document.number.value)
                self?.stackView.addArrangedSubview(numberView)
            }
        }
    }
}
