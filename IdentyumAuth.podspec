Pod::Spec.new do |s|
  s.name             = 'IdentyumAuth'
  s.version          = '0.2.0'
  s.summary          = 'IdentyumAuth for iOS is a client SDK for communicating with OAuth 2.0 and OpenID Connect providers.'
 
  s.description      = <<-DESC
IdentyumAuth for iOS is a client SDK for communicating with [OAuth 2.0]
(https://tools.ietf.org/html/rfc6749) and [OpenID Connect]
(http://openid.net/specs/openid-connect-core-1_0.html) providers. It strives to
directly map the requests and responses of those specifications, while following
the idiomatic style of the implementation language. In addition to mapping the
raw protocol flows, convenience methods are available to assist with common
tasks like performing an action with fresh tokens.
It follows the OAuth 2.0 for Native Apps best current practice
([RFC 8252](https://tools.ietf.org/html/rfc8252)).
                       DESC
 
  s.homepage         = 'https://bitbucket.org/identyum/identyum-auth-ios/'
  s.license          = 'Apache License, Version 2.0'
  s.author           = { 'Katarina Pelicaric' => 'katarina.pelicaric@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/identyum/identyum-auth-ios/', :tag => s.version }
 
  s.ios.deployment_target = '10.0'
  s.source_files = 'IdentyumAuth/*.swift'
  s.dependency 'AppAuth'
 
end